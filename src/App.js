import React from 'react';
import Navbar from './components/navbar/Navbar';
import Sidebar from './components/sidebar/Sidebar'
import {Route, Switch, useLocation} from 'react-router-dom'
import About from './components/about/About'
import Projects from './components/projects/Projects'
import Resume from './components/resume/Resume'
import {AnimatePresence} from 'framer-motion' 

function App() {

  // location hook so that AnimatePresence will know when the route is switched
  const location = useLocation();
  return (
      <div className="app">
        <div className="container app__container">
          <div className="row app__row">
            <div className="col-lg-3">
                <Sidebar />
            </div>
            <div className="col-lg-9 app__main-content">
              
              {/* navbar */}
              <Navbar />
              <AnimatePresence exitBeforeEnter>
                <Switch location={location} key={location.key}>
                  <Route exact path="/"  component={About} />
                  <Route path="/resume"  component={Resume} />
                  <Route path="/projects"  component={Projects} />
                </Switch>
              </AnimatePresence>
            </div>
          </div>
        </div>
      </div>
  );
}

export default App;