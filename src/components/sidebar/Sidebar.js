import React from 'react'

import facebook from '../../assets/icons/facebook.svg'
import instagram from '../../assets/icons/instagram.svg'
import gitlab from '../../assets/icons/gitlab.svg'
import pin from '../../assets/icons/pin.svg'
import resume_icon from '../../assets/icons/resume_icon.svg'
import profilepicture from '../../assets/profilepicture.JPG'
import resume from '../../assets/resume.pdf'
import {motion} from 'framer-motion'
import './sidebar.css'

const Sidebar = () => {

        const handleEmailMe=( ) => {
            window.open("mailto:amandatampoc@gmail.com")
        }

        const sidebar_variant = {
            hidden: {
                x: '-20vw',
    
            },
            visible: {
                x: 0,
    
                transition: {
                    delay: 0.1, duration: 0.5, type: 'spring'
                }
            }
        }
    
return (
        <motion.div className="sidebar"
            variants={sidebar_variant}
            initial='hidden'
            animate="visible"
        >
            {/* use curly braces when implementing javascript inside html */}
            <img src={profilepicture} alt="avatar" className="sidebar__avatar"/>
            <div className="sidebar__name">Amanda <span> Grace</span></div>
            <div className="sidebar__item sidebar__title">SAP Consultant </div>
            <a href={resume} download="resume">
                <div className="sidebar__item sidebar__resume">
                    <img src={resume_icon} alt="" className="sidebar__icon" /> Download Resume
                </div>
            </a>
            <figure className="sidebar__social-icons my-2">
                <a href="https://www.facebook.com/chortlers/"><img src={facebook} alt="facebook" className="sidebar__icon mr-3"/></a>
                <a href="instagram.com/amanda_grace.8/"><img src={instagram} alt="instagram" className="sidebar__icon"/></a>
            </figure>
            <div className="sidebar__contact">
                <div className="sidebar__item sidebar__github">
                    <a href="/#"><img src={gitlab} alt="gitlab" className="sidebar__icon mr-3"/> gitlab</a>
                </div>
                <div className="sidebar__location">
                    <img src={pin} alt="location" className="sidebar__icon mr-3"/>
                    Manila, Philippines</div>
                <div className="sidebar__item">amandatampoc@gmail.com</div>
            </div>
            <div className="sidebar__item sidebar__email" onClick={handleEmailMe}>Email me</div>
        </motion.div>
    )
}

export default Sidebar;