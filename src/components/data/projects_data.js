import Books from '../../assets/projects_images/Books.png'
import portfolio from '../../assets/projects_images/portfolio.png'

const data_projects = [
    {
        name: 'Library Management System',
        image: Books,
        deployed_url: 'https://b45-library-csp2-project.herokuapp.com/login',
        gitlab_url: 'https://b45-library-csp2-project.herokuapp.com/login',
        category: ['laravel']
    },

    {
        name: 'Portfolio',
        image: portfolio,
        deployed_url: 'https://webdevportfolio-tampoc.netlify.app',
        gitlab_url: 'https://webdevportfolio-tampoc.netlify.app',
        category: [ 'mongoDB', 'react.js', 'node.js']
    }
]

export default data_projects;