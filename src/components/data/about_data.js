import React from 'react';

import webdesign from '../../assets/icons/webdesign.svg'
import fullstack from '../../assets/icons/fullstack.svg'
import management from '../../assets/icons/management.svg'
import consulting from '../../assets/icons/consulting.svg'

const skills = [
    {
        icon:consulting,
        title: "SAP Consulting",
        about: ["Deliver ", <strong key={1}>SAP Sales and Distribution</strong>, " solutions within the SLA (Service Level Agreement). Engage in problem resolution with the use of multiple technologies like", <strong key={1}> SAP, Unix, Tidal, and HPSB.</strong> ]
    },
    {
        icon:management,
        title: "Incident Escalation Management",
        about: ["Facilitate fast resolution of escalated and elevated tickets to avoid further escalations. Provide and raise gaps and improvement opportunities related to other  ",<strong key={1}> ITIL (IT Infrastructure Library) processes </strong>]
    },
    {
        icon:fullstack,
        title: "Full Stack Development",
        about: [ "Develop web-based application using " ,<strong key={1}>CSS3, HTML5 and Javascript</strong>, " for frontend and deployment in the backend using",<strong key={1}> MongoDB, Node, Express</strong>,  " Please refer to the projects section for sample works"]
    },
    
   
    {
        icon:webdesign,
        title: "Web Design",
        about: "Conceptualize original website design ideas that bring simplicity and user friendliness to complex roadblocks. Create wireframes, storyboards, process flows and site maps to communicate interaction and design ideas."
    },
   
]

export default skills;