import react from '../../assets/icons/react.svg'
import javascript from '../../assets/icons/java-script.svg'
import html from '../../assets/icons/html.svg'
import sap from '../../assets/icons/sap.svg'
import vercel from '../../assets/icons/vercel.svg'
import heroku from '../../assets/icons/heroku.svg'
import unix from '../../assets/icons/unix.svg'
import cisco from '../../assets/icons/cisco.svg'
import mongodb from '../../assets/icons/mongodb.svg'
import laravel from '../../assets/icons/laravel.svg'
import mysql from '../../assets/icons/mysql.svg'
import jquery from '../../assets/icons/jquery.svg'
import itil from '../../assets/icons/itil.svg'

const languages =[
    {
        icon:javascript,
        name:'Javascript',
        level: '50'
    },
    {
        icon:html,
        name:'HTML',
        level: '60'
    },
    {
        icon:sap,
        name:'ABAP',
        level: '30'
    },
    {
        icon:mysql,
        name:'MySQL',
        level: '30'
    },
    {
        icon:jquery,
        name:'jQuery',
        level: '30'
    },
    {
        icon:react,
        name:'React Native',
        level: '70'
    },
    {
        icon:laravel,
        name:'Laravel',
        level: '70'
    },
    {
        icon:mongodb,
        name:'MongoDB',
        level: '60'
    },
    {
        icon:itil,
        name:'ITIL',
        level: '95'
    }
]

const tools =[
    {
        icon:sap,
        name:'SAP',
        level: '75'
    },
    {
        icon:cisco,
        name:'Tidal',
        level: '90'
    },
    {
        icon:unix,
        name:'Unix',
        level: '50'
    },
    {
        icon:heroku,
        name:'Heroku',
        level: '60'
    },
    {
        icon:vercel,
        name:'Vercel',
        level: '60'
    }
]

export {tools, languages}